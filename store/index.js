import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
    return new Vuex.Store({
        state: {
            selectedRegion: [],
            selectedCountry: []
        },
        mutations: {
            getRegion(state, option) {
                state.selectedRegion = option

            },
            getCountry(state, option) {
                state.selectedCountry = option
                
            }
        },
        actions: {
            nuxtServerInit(vuexContext, context) {
                return axios.get('https://restcountries.eu/rest/v2/all')
                    .then(res => {
                        const postsArray = []
                        for (res.data.alpha3Code in res.data) {
                            postsArray.push({ ...res.data[res.data.alpha3Code], id: res.data.alpha3Code})
                        }
                        vuexContext.commit('getRegion', postsArray)
                    })
                    .catch(e => context.error(e));
            },
            getAll(vuexContext, context) {
                vuexContext.commit('getCountry', [])
                
                return axios
                    .get('https://restcountries.eu/rest/v2/all')
                    .then(res => {
                        const postsArray = []
                        for (res.data.alpha3Code in res.data) {
                            postsArray.push({ ...res.data[res.data.alpha3Code], id: res.data.alpha3Code})
                        }
                        vuexContext.commit('getRegion', postsArray)
                    })
                    .catch(e => context.error(e));
            },
            getRegion(vuexContext, region) {
                vuexContext.commit('getCountry', [])

                return axios
                    .get('https://restcountries.eu/rest/v2/region/' + region)
                    .then((res) => {
                        const countryInfo = []
                        for (const key in res.data) {
                            countryInfo.push({ ...res.data[key], id: key })
                        }
                        vuexContext.commit('getRegion', countryInfo)
                    })
                    .catch((e) => context.error(e))
            },
            getCountry(vuexContext, country) {
                return axios
                    .get('https://restcountries.eu/rest/v2/alpha/' + country)
                    .then((res) => {
                        vuexContext.commit('getCountry', [res.data])
                    })
                    .catch((e) => context.error(e))
            },
        },
        getters: {
            selectedRegion(state) {
                return state.selectedRegion
            },
            selectedCountry(state) {
                return state.selectedCountry
            }
        },
    })
}

export default createStore
