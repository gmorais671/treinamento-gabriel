import Vue from 'vue'

import Header from '@/components/header/Header'
import SearchSection from '~/components/searchSection/SearchSection'
import FlagsSection from '~/components/flagsSection/FlagsSection'
import Flag from '~/components/flagsSection/Flag'
import CountrySection from '~/components/countrySection/CountrySection'
import BorderCountriesSection from '~/components/borderCountriesSection/BorderCountriesSection'
import CountryFlag from '~/components/borderCountriesSection/CountryFlag'

Vue.component('Header', Header)
Vue.component('SearchSection', SearchSection)
Vue.component('FlagsSection', FlagsSection)
Vue.component('Flag', Flag)
Vue.component('CountrySection', CountrySection)
Vue.component('BorderCountriesSection', BorderCountriesSection)
Vue.component('CountryFlag', CountryFlag)